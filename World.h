#ifndef WORLD_H
#define WORLD_H

#include <iostream>
#include <random>

#define MAX_TILES_W 160
#define MAX_TILES_H 100
#define CHANCE_ALIVE 4.5f

using array2D = std::vector< std::vector< int > >;

class World {

public:

	World();
	~World();

	int initializeMap();

	int testMap();

	// Getting the first evolution of the map
	array2D getWorld(array2D &array);

	// Doing evolution steps to generate smoother better looking map
	int countAliveNeighbours(array2D &array, int x, int y);

	array2D evolutionStep(array2D &oldWorld, int deathLimit, int birthLimit);
	array2D putDiamonds(array2D &oldWorld, int treashureLimit);

private:

	std::default_random_engine e{};
	std::uniform_int_distribution<unsigned long> d{ 1, 10 };
	
	int cellmap[MAX_TILES_W][MAX_TILES_H];

	
};



#endif	// WORLD_H