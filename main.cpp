#include <iostream>

#include "Window.h"



constexpr unsigned int WINDOW_WIDTH = 1280;
constexpr unsigned int WINDOW_HEIGHT = 720;

int main()
{
	
	Window window(WINDOW_WIDTH, WINDOW_HEIGHT, "Cellular automation test");

	return 0;
}