#include "Window.h"


Window::Window(unsigned int width, unsigned int height, std::string name) :
	windowWidth{ width }, 
	windowHeight{ height },
	windowName{ name }
{

	Setup();

}

int Window::Setup() {

	window.create(sf::VideoMode(windowWidth, windowHeight), windowName, sf::Style::Titlebar | sf::Style::Close);
	world.initializeMap();
	std::cout << "testing map" << std::endl;
	world.testMap();

	world.getWorld(map);

	std::cout << "********************************" << std::endl;
	std::cout << "Window::map output:" << std::endl;
	std::cout << "********************************" << std::endl;

	for (int i = 0; i < MAX_TILES_W; i++) {
		for (int j = 0; j < MAX_TILES_H; j++) {
			std::cout << map[i][j];
		}
		std::cout << "\n";
	}

	

	// sky texture
	skybox.loadFromFile("sky.png");
	sky.setTexture(skybox);

	// tile textures
	tileTexture.loadFromFile("dirt.png");
	tiles.setTexture(tileTexture);
	wallTexture.loadFromFile("wall.png");
	walls.setTexture(wallTexture);
	grassTexture.loadFromFile("grass.png");
	grass.setTexture(grassTexture);

	

	

	Start();
	

	return EXIT_SUCCESS;
}

void Window::Start() {

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event))
		{

			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black);
		
		
		elapsed = clock.getElapsedTime();
		
		
		if (k < elapsed.asSeconds()) {
			map = world.evolutionStep(map, 2, 4);
			k = elapsed.asSeconds() + 1;
		}
		
		if (elapsed.asSeconds() > 10) {
			grassTexture.loadFromFile("snow.png");
		}

		//sky
		sky.setScale(1.6, 1);
		sky.setPosition(0, 0);
		window.draw(sky);

		// Drawing tiles
		for (int i = 0; i < MAX_TILES_W; i++) {
			for (int j = 0; j < MAX_TILES_H; j++) {
				if (map[i][j] > 0) {
					tiles.setPosition(i * 8, 400 + j * 8);
					//std::cout << "X Possition: " << i * 16 << "\nY Possition: " << j * 16 << std::endl;
					window.draw(tiles);
					if (j < 2) {
						grass.setPosition(i * 8, 400 + j * 8);
						window.draw(grass);
					}
				}
				else if(map[i][j] == 0){
					walls.setPosition(i * 8, 400 + j * 8);
					window.draw(walls);
				}
			}
		}
		//
	
		
		window.display();
	}

}

Window::~Window() {

}